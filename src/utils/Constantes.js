import Banner from '../img/banner.jpg'
import Diente from '../img/diente.svg'

export default {
    IMAGENES: {
        'banner': Banner,
        'diente': Diente
    },
    CATALOGO_PRODUCTOS: 'SET_PRODUCTOS',
    CATALOGO_SERVICIOS: 'SET_SERVICIOS',
    CATALOGO_DESCUENTO: 'SET_DESCUENTO',
    mapsKey: 'AIzaSyC8TfGROrwmRvhvs1yukJ9YtLFAkNWizzs'
}
<?php
    header('Content-Type: application/json; charset=utf-8');
    header('access-control-allow-origin: *');

    if(isset($_GET['info'])){
        $productos = [
            1=>['nombre'=>'Empaste', 'costos'=>[1=>['nombre'=>'Ordinario', 'valor'=>100], 2=>['nombre'=>'VIP', 'valor'=>500], 3=>['nombre'=>'Urgencias', 'valor'=>300]]],
            2=>['nombre'=>'Brackets', 'costos'=>[1=>['nombre'=>'Ordinario', 'valor'=>2000], 2=>['nombre'=>'VIP', 'valor'=>4500], 3=>['nombre'=>'Urgencias', 'valor'=>1000]]],
            3=>['nombre'=>'Extracción', 'costos'=>[1=>['nombre'=>'Ordinario', 'valor'=>500], 2=>['nombre'=>'VIP', 'valor'=>1500], 3=>['nombre'=>'Urgencias', 'valor'=>750]]],
            4=>['nombre'=>'Limpieza', 'costos'=>[1=>['nombre'=>'Ordinario', 'valor'=>500], 2=>['nombre'=>'VIP', 'valor'=>2000], 3=>['nombre'=>'Luxury', 'valor'=>3200], 4=>['nombre'=>'Urgencias', 'valor'=>700]]],
            5=>['nombre'=>'Cirugía', 'costos'=>[1=>['nombre'=>'Ordinario', 'valor'=>3000], 2=>['nombre'=>'VIP', 'valor'=>5000], 3=>['nombre'=>'Urgencias', 'valor'=>5000]]],
            6=>['nombre'=>'Implante', 'costos'=>[1=>['nombre'=>'Ordinario', 'valor'=>10000], 2=>['nombre'=>'VIP', 'valor'=>20000], 3=>['nombre'=>'Urgencias', 'valor'=>15475]]]
        ];
        $servicios = [1=>'Ordinario', 2=>'VIP', 3=>'Urgencias'];
        $descuentos = ['LaFer'=>50,'Meny'=>50,'JonasBrothers'=>30,'Azul'=>10];

        switch($_GET['info']){
            case 'catalogo':
                $retorno = ['servicios'=>$servicios, 'productos'=>[]];

                foreach($productos as $index=>$val)
                    $retorno['productos'][$index] = $val['nombre'];
                break;
            case 'cotiza':
                if(array_key_exists($_GET['producto'], $productos))
                    $retorno = ['servicios'=>$productos[$_GET['producto']], 'descuento'=>(!isset($_GET['descuento']) || !array_key_exists($_GET['descuento'], $descuentos)) ? false : $descuentos[$_GET['descuento']]];
                else
                    $retorno = [];
                break;
            default:
                $retorno = [];
                break;
        }

        echo json_encode($retorno);
    }
?>
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { Provider } from 'react-redux';

import store from './store';
import Headerbar from './componentes/Headerbar';
import Inicio from './pages/Inicio';
import QuienesSomos from './pages/QuienesSomos';
import Encuentranos from './pages/Encuentranos';
import Cotiza from './pages/Cotiza';

import './App.css';

function App() {
	return (
		<Provider store={store}>
			<div className="App">
				<BrowserRouter>
					<Headerbar />
					<Routes>
						<Route path="/" element={<Inicio />} />
						<Route path="/quienessomos" element={<QuienesSomos />} />
						<Route path="/encuentranos" element={<Encuentranos />} />
						<Route path="/cotiza" element={<Cotiza />} />
					</Routes>
				</BrowserRouter>
			</div>
		</Provider>
	);
}

export default App;
import { Engineering, Visibility, Diamond, ThumbUp } from '@mui/icons-material';
import { Container, Grid, List, ListItem, ListItemIcon, ListItemText } from '@mui/material';

import '../css/QuienesSomos.css';

function QuienesSomos () {
    const listValores = ['Integridad y Respeto', 'Compromiso y Disciplina', 'Excelencia en el Servicio', 'Calidez y Calidad', 'Mejora Continua'];

    return (
        <Container>
            <Grid container spacing={3}>
                <Grid item xs={12} className="titulo">
                    <h1>¿Quienes somos?</h1>
                </Grid>
                <Grid item xs={12}>
                    <p>Empresa 100% mexicana que brinda atención y servicio para mejorar la salud bucodental de nuestros pacientes,
                    donde podrás encontrar todas las especialidades dentales en un sólo lugar recibiendo el mejor trato y
                    una atención personalizada. Iniciamos en 1994 como concepto innovador de atención en servicios dentales
                    combinando la excelencia en tratamientos y calidad en el servicio otorgado por nuestro personal médico,
                    especialistas y colaboradores. Más de 26 años de experiencia en el sector nos respaldan, somos una red
                    compuesta por 29 clínicas en Monterrey y 1 en Nuevo Laredo con un equipo sólido de dentistas y especialistas
                    en el área de odontología.</p>
                    <p>En 2006, Imagen Dental fue la primera empresa del sector salud en México seleccionada por la organización
                    internacional Endeavor, organismo sin fines de lucro, que impulsa y asesora a emprendedores de alto impacto
                    para detonar el crecimiento económico en diferentes industrias, comunidades y países.</p>
                </Grid>
                <Grid item md={4} xs={12}>
                    <Grid item xs={12}>
                        <Engineering sx={{ fontSize: 70, color: "#1976d2" }} />
                    </Grid>
                    <Grid item xs={12} className="titulo2">Misión</Grid>
                    <Grid item xs={12}>
                        <p>Mejorar la calidad de vida de nuestros pacientes brindando servicios de calidad integral y accesible,
                        buscando soluciones innovadoras y un servicio diferenciado.</p>
                    </Grid>
                </Grid>
                <Grid item md={4} xs={12}>
                    <Grid item xs={12}>
                        <Visibility sx={{ fontSize: 70, color: "#1976d2" }} />
                    </Grid>
                    <Grid item xs={12} className="titulo2">Visión</Grid>
                    <Grid item xs={12}>
                        <p>Establecernos como la opción número uno en servicios dentales en el país, otorgando calidad a nuestros
                        pacientes excediendo sus expectativas y necesidades, siempre cuidando su bienestar y el de su familia.</p>
                    </Grid>
                </Grid>
                <Grid item md={4} xs={12}>
                    <Grid item xs={12}>
                        <Diamond sx={{ fontSize: 70, color: "#1976d2" }} />
                    </Grid>
                    <Grid item xs={12} className="titulo2">Valores</Grid>
                    <Grid item xs={12}>
                        <div className="listaValores">
                            <List>
                                {listValores.map((valor) => (
                                    <ListItem key={valor} sx={{ my: -2 }}>
                                        <ListItemIcon>
                                            <ThumbUp sx={{ color: "#1976d2" }} />
                                        </ListItemIcon>
                                        <ListItemText
                                            primary={valor}
                                        />
                                    </ListItem>
                                ))}
                            </List>
                        </div>
                    </Grid>
                </Grid>
            </Grid>
        </Container>
   );
};

export default QuienesSomos;
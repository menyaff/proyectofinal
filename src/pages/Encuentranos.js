import { useState } from 'react';
import { Container, Grid, InputAdornment, Button } from '@mui/material';
import { WhatsApp, Instagram, Twitter, Email, Facebook } from '@mui/icons-material';
import { makeStyles } from '@material-ui/core/styles';
import Link from '@mui/material/Link';
import Modal from '@material-ui/core/Modal';
import TextField from '@material-ui/core/TextField';

import Constantes from '../utils/Constantes.js'
import Map from '../componentes/Map';

import '../css/Encuentranos.css';

function Encuentranos() {
    const MapUrl = `https://maps.googleapis.com/maps/api/js?v=3.exp&key=${Constantes.mapsKey}`;

    const [nombre, setNombre]= useState('');
    const [comentario, setComentario]= useState('');
    const [modalStyle] = useState(getModalStyle);
    const [open, setOpen] = useState(false);

    function rand() {
        return Math.round(Math.random() * 20) - 10;
    }

    function getModalStyle() {
        const top = 50 + rand();
        const left = 50 + rand();

        return {
         top: `${top}%`,
         left: `${left}%`,
         transform: `translate(-${top}%, -${left}%)`,
        };
    }

    const useStyles = makeStyles((theme) => ({
        paper: {
            position: 'absolute',
            width: 400,
            backgroundColor: theme.palette.background.paper,
            border: '2px solid #000',
            boxShadow: theme.shadows[5],
            padding: theme.spacing(2, 4, 3),
        },
    }));
    
    const classes = useStyles();

    function handleModal(event, status) {
		event.preventDefault();

        if (status==true){
            setOpen(true);
        }else{
            setOpen(false);
            setNombre('');
            setComentario('');
        }
	}

    const handleInputNombreChange = (event) => {
        setNombre(event.target.value);
    } 

    const handleInputComentarioChange = (event) => {
        setComentario(event.target.value);
    } 

    const body = (
        <div style={modalStyle} className={classes.paper}>
          <h2 id="simple-modal-title">Comentario</h2>
           Hola {nombre}, tu comentario "{comentario}" se envió con éxito.
        </div>
    );

    return (
        <Container>
            <Grid container spacing={2}>
                <Grid item xs={12} md={6}>
                    <Grid item xs={2} fontSize="30px">Encuéntranos</Grid>
                    <Map 
                        googleMapURL= {MapUrl}
                        containerElement= {<div style = {{height: '200px'}}/>}
                        mapElement= {<div style={{height: '50%'}}/>}
                        loadingElement= {<p>Cargando</p>}
                    />
                </Grid>
                <Grid item xs={12} md={6}>
                    <Grid container spacing={2}>
                        <Grid item xs={4} fontSize="30px">Contáctanos</Grid>
                    </Grid>
                    <Grid container spacing={2} className="redSocial">
                        <Grid item xs={1}>
                            <WhatsApp sx={{ fontSize: 30, color: "#1976d2" }} />
                        </Grid>
                        <Grid item xs={1}>
                            6691104725
                        </Grid>
                    </Grid>
                    <Grid container spacing={2} className="redSocial">
                        <Grid item xs={1}>
                            <Instagram sx={{ fontSize: 30, color: "#1976d2" }} />
                        </Grid>
                        <Grid item xs={1} >
                            <Link href='https://www.instagram.com/ferm97a/'>@ferm97a</Link>
                        </Grid>
                    </Grid>
                    <Grid container spacing={2} className="redSocial">
                        <Grid item xs={1}>
                            <Twitter sx={{ fontSize: 30, color: "#1976d2" }} />
                        </Grid>
                        <Grid item xs={1}>
                            <Link href='https://twitter.com/fermurillo_a'>@fermurillo_a</Link>
                        </Grid>
                    </Grid>
                    <Grid container spacing={2} className="facebook">
                        <Grid item xs={1}>
                            <Facebook sx={{ fontSize: 30, color: "#1976d2" }} />
                        </Grid>
                        <Grid item xs={1}>
                            <Link href='https://www.facebook.com/Fermurillo97'>Fermurillo97</Link>
                        </Grid>
                    </Grid>
                    <Grid container spacing={2} className="correo">
                        <Grid item xs={3}>
                            <TextField
                                id="input-with-icon-textfield"
                                label="Nombre"
                                onChange={handleInputNombreChange}
                                InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                            <Email />
                                        </InputAdornment>
                                    ),
                                }}
                                variant="standard"
                                value={nombre}
                            />
                        </Grid>
                        <Grid item xs={4}>
                            <TextField id="input-with-icon-grid" value = {comentario} label="Comentario" onChange={handleInputComentarioChange} />
                        </Grid>
                        <Grid item xs={12}>
                            <Button variant="contained" component="span" onClick={ev => handleModal(ev, true)}>
                               Enviar
                            </Button>
                            <Modal
                                open={open}
                                onClose={ev => handleModal(ev, false)}
                                aria-labelledby="simple-modal-title"
                                aria-describedby="simple-modal-description"
                            >
                                {body}
                            </Modal>
                        </Grid>
                    </Grid>
                </Grid>
            </Grid>    
        </Container>
	);
};

export default Encuentranos;
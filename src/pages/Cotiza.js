import { useEffect, useState } from "react";
import { useSelector, useDispatch } from 'react-redux';
import { FormControl, InputLabel, Select, MenuItem, Input, InputAdornment, TextField, Container, Grid } from '@mui/material';

import Constantes from '../utils/Constantes.js'
import { getCatalogos, setDescuento } from '../store/actions';

import '../css/Cotiza.css';

function Cotiza () {
    const productos = useSelector(store => store.productos);
    const servicios = useSelector(store => store.servicios);
    const descuento = useSelector(store => store.descuento);
    const dispatch = useDispatch();
    const [producto, setProducto]= useState('0');
    const [servicio, setServicio]= useState('0');
    const [codigoDescuento, setCodigoDescuento]= useState('');
    const [total, setTotal]= useState('0');

    const calculaTotal = () => {
        setTotal(!descuento ? servicios[servicio]['valor'] : servicios[servicio]['valor'] - (servicios[servicio]['valor']*(descuento / 100)));
    };

    useEffect(() => {
        dispatch(getCatalogos(Constantes.CATALOGO_PRODUCTOS, null));
    }, []);

    useEffect(() => {
        if(producto != 0){
            dispatch(getCatalogos(Constantes.CATALOGO_SERVICIOS, {producto: producto, codigoDescuento: codigoDescuento}));
            setServicio(0);
        }
    }, [producto]);

    useEffect(() => {
        if(servicio != 0)
            calculaTotal();
    }, [servicio, descuento]);

	function handleProductos(event){
        setProducto(event.target.value);
        setTotal(0);
	};

	function handleServicios(event){
        setServicio(event.target.value);
	};    

	function handleCodigoDescuento(event){
        event.preventDefault();

        if(producto != 0)
            dispatch(getCatalogos(Constantes.CATALOGO_SERVICIOS, {producto: producto, codigoDescuento: codigoDescuento}));
	};  

    return(
        <div>
            <Container>
                <Grid container spacing={1}>
                    <Grid item xs={8}></Grid>
                    <Grid item xs={4}>
                        <form onSubmit={handleCodigoDescuento}>
                            <TextField id="standard-basic" label="Código de descuento" variant="standard" className="fieldCodigo" onChange={(ev) => {setCodigoDescuento(ev.target.value)}} />
                        </form>
                    </Grid>
                    <Grid item xs={8}></Grid>
                    <Grid item xs={4}>
                        <form onSubmit={handleCodigoDescuento}>
                            {descuento}%
                        </form>
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }} >
                            <InputLabel id="demo-simple-select-standard-label">Productos</InputLabel>
                            <Select
                                labelId="demo-simple-select-standard-label"
                                id="demo-simple-select-standard"
                                value={producto}
                                onChange={handleProductos}
                                label="Productos"
                            >
                                <MenuItem value=""></MenuItem>
                                {Object.keys(productos).map((key) => (
                                    <MenuItem key={key} value={key}>{productos[key]}</MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={12} md={6}>
                        <FormControl variant="standard" sx={{ m: 1, minWidth: 120 }} >
                            <InputLabel id="demo-simple-select-standard-label">Servicios</InputLabel>
                            <Select
                                labelId="demo-simple-select-standard-label"
                                id="demo-simple-select-standard"
                                value={servicio}
                                onChange={handleServicios}
                                label="Servicios"
                            >
                                <MenuItem value=""></MenuItem>
                                {Object.keys(servicios).map((key) => (
                                    <MenuItem key={key} value={key}>{servicios[key]['nombre']}</MenuItem>
                                ))}
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={12}>
                        <div className="App-intro">
                            <img src={Constantes.IMAGENES.diente} className="dientitoFeliz" alt="logo" />
                        </div>
                    </Grid>
                    <Grid item xs={12}>
                        <div className="formTotal">
                            <FormControl  sx={{ m: 1 }} variant="standard">
                                <InputLabel htmlFor="standard-adornment-amount" className="inputTotal">Total</InputLabel>
                                    <Input
                                        id="standard-adornment-amount"
                                        value={total}
                                        startAdornment={<InputAdornment position="start">$</InputAdornment>}
                                    />
                            </FormControl>
                        </div>
                    </Grid>
                </Grid>
            </Container>
        </div>
    );
};

export default Cotiza;
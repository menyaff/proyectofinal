import Constantes from '../utils/Constantes.js'

import '../css/Inicio.css';

function Inicio() {
	return (
        <div className="App-intro">
            <img src={Constantes.IMAGENES.diente} className="App-logo" alt="logo" />	
            <img src={Constantes.IMAGENES.banner} className="App-Banner" alt="banner"  />
            <h1 className="texto">Bienvenido</h1>
        </div>
	);
}

export default Inicio;
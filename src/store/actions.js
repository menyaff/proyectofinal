import axios from 'axios';

import Constantes from '../utils/Constantes.js'

export function setProductos(info) {
    return { type: Constantes.CATALOGO_PRODUCTOS, payload: info };
}
export function setServicios(info) {
    return { type: Constantes.CATALOGO_SERVICIOS, payload: info };
}
export function setDescuento(info) {
    return { type: Constantes.CATALOGO_DESCUENTO, payload: info };
}
export const getCatalogos = (elem, info) => async(dispatch) => {
    switch(elem){
        case Constantes.CATALOGO_PRODUCTOS:
            var response = await axios({
                url: `http://localhost/apireact/API.php?info=catalogo`,
                method: 'GET'
            });

            dispatch(setProductos(response.data.productos));
            break;
        case Constantes.CATALOGO_SERVICIOS:
            var response = await axios({
                url: `http://localhost/apireact/API.php?info=cotiza&producto=${info.producto}&descuento=${info.codigoDescuento}`,
                method: 'GET'
            });

            dispatch(setServicios(response.data.servicios.costos));
            dispatch(setDescuento(!response.data.descuento ? 0 : response.data.descuento));
            break;
    }
};
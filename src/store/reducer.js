import Constantes from '../utils/Constantes.js'

const initialState = {
    productos: [],
    servicios: [],
    descuento: 0
}

function reducer(state = initialState, action) {
    const { type, payload } = action;

    switch(type) {
        case Constantes.CATALOGO_PRODUCTOS:{
            return {
                ...state,
                productos: payload
            }
        }
        case Constantes.CATALOGO_SERVICIOS:{
            return {
                ...state,
                servicios: payload
            }
        }
        case Constantes.CATALOGO_DESCUENTO:{
            return {
                ...state,
                descuento: payload
            }
        }
        default:{
            return {
                ...state,
                productos: state.productos,
                servicios: state.servicios,
                descuento: state.descuento
            }
        }
    }
}

export default reducer;
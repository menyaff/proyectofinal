import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from '@redux-devtools/extension'
import thunk from 'redux-thunk';
import reducer from './reducer';

const store = createStore(
    reducer, 
    composeWithDevTools(
        applyMiddleware(thunk)
    )
    //window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);

export default store;
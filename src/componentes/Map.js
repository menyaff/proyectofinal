import React from 'react'
import { GoogleMap, useJsApiLoader } from '@react-google-maps/api';
import Constantes from '../utils/Constantes';

const containerStyle = {
  width: '400px',
  height: '400px'
};

const center = {
  lat: 23.23227442812453,
  lng: -106.40370140850428
};
 
function MyComponent() {
  const { isLoaded } = useJsApiLoader({
    id: 'google-map-script',
    googleMapsApiKey: Constantes.mapsKey
  })

  return isLoaded ? (
        <GoogleMap
            mapContainerStyle={containerStyle}
            center={center}
            zoom={20}
        >
        </GoogleMap>
  ) : <></>
}

export default React.memo(MyComponent)
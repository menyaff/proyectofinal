import MenuIcon from '@mui/icons-material/Menu';

import { AppBar, Container, Box, Toolbar, Button, IconButton, Menu, MenuItem, Typography } from '@mui/material';

import { useState } from "react";
import { useNavigate } from 'react-router-dom';

import '../css/Headerbar.css';

function Headerbar(){
	const [navmenu, setNavmenu] = useState(null);
	const navigate = useNavigate();

	const handleShowNavmenu = (event) => {
		setNavmenu(event.currentTarget);
	};

	const handleHideNavmenu = () => {
		setNavmenu(null);
	};

	function navigateTo(event, path) {
		event.preventDefault();

		setNavmenu(null);
		navigate(path)
	}

    return (		
		<AppBar position="static" className="menuPrincipal">
			<Container maxWidth="xl">
				<Toolbar disableGutters>
					<Box sx={{ flexGrow:1, display: { xs: 'flex', sm: 'none'}}}>
						<IconButton
							size="large"
							aria-label="account of current user"
							aria-controls="menu-appbar"
							aria-haspopup="true"
							onClick={handleShowNavmenu}
							color="inherit"
							sx={{ mr: 2}}
						>
							<MenuIcon />
						</IconButton>
						<Menu
							id="menu-appbar"
							anchorEl={navmenu}
							anchorOrigin={{
								vertical: 'bottom',
								horizontal: 'left',
							}}
							keepMounted
							transformOrigin={{
								vertical: 'top',
								horizontal: 'left',
							}}
							open={Boolean(navmenu)}
							onClose={handleHideNavmenu}
						>
							<MenuItem key="Inicio" onClick={ev => navigateTo(ev, '')}>
								<Typography textAlign="center">Inicio</Typography>
							</MenuItem>
							<MenuItem key="¿Quiénes somos?" onClick={ev => navigateTo(ev, 'quienessomos')}>
								<Typography textAlign="center">¿Quiénes somos?</Typography>
							</MenuItem>
							<MenuItem key="Cotiza" onClick={ev => navigateTo(ev, 'cotiza')}>
								<Typography textAlign="center">Cotiza</Typography>
							</MenuItem>
							<MenuItem key="Encuéntranos" onClick={ev => navigateTo(ev, 'encuentranos')}>
								<Typography textAlign="center">Encuéntranos</Typography>
							</MenuItem>
						</Menu>
					</Box>
					<Box sx={{display: { xs: 'none', sm: 'flex'}}}>
						<Button
								onClick={ev => navigateTo(ev, '')}
								sx={{ my: 2, color: 'white' }}
							>
							Inicio
						</Button>
						<Button
							onClick={ev => navigateTo(ev, 'quienessomos')}
							sx={{ my: 2, color: 'white' }}
						>
							¿Quiénes somos?
						</Button>
						<Button
							onClick={ev => navigateTo(ev, 'cotiza')}
							sx={{ my: 2, color: 'white' }}
						>
							Cotiza
						</Button>
						<Button
							onClick={ev => navigateTo(ev, 'encuentranos')}
							sx={{ my: 2, color: 'white' }}
						>
							Encuéntranos
						</Button>
					</Box>
				</Toolbar>
			</Container>
		</AppBar>
    );
}

export default Headerbar;